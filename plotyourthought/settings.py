"""
Django settings for plotyourthought project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""

import sendgrid
SEND_GRID = sendgrid.SendGridClient('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')

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '+mphw!_8z@7^1$rs9&2h^g2m4pu+c3w5+yu(%mz9u50_+ui8vz'

PRODUCTION = False

if PRODUCTION:
    # SECURITY WARNING: don't run with debug turned on in production!
    DEBUG = False
else:
    DEBUG = True

# Templates-related settings
# https://docs.djangoproject.com/en/1.7/ref/settings/#template-context-processors

if PRODUCTION:
    TEMPLATE_DEBUG = False
else:
    TEMPLATE_DEBUG = True

TEMPLATE_DIRS = (
    os.path.join(os.path.dirname(__file__), 'template'),
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "social.apps.django_app.context_processors.backends",
    "social.apps.django_app.context_processors.login_redirect",
)

# Allowed hosts
# https://docs.djangoproject.com/en/1.7/ref/settings/#allowed-hosts

if PRODUCTION:
    ALLOWED_HOSTS = [
        'plotyourthought.com',
        'plotyourthought.com.'
    ]
else:
    ALLOWED_HOSTS = []

# Authentication-related settings
# https://docs.djangoproject.com/en/1.7/ref/settings/#auth
# https://python-social-auth.readthedocs.org/en/latest/configuration/settings.html
# https://python-social-auth.readthedocs.org/en/latest/configuration/django.html

AUTH_USER_MODEL = 'app.User'

if PRODUCTION:
    SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '614065154267-i506ncfo8afcie6e6qr2fboc7k5d24ke.apps.googleusercontent.com'
    SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'kGcWsGL1J-LjYKIA-3mtHk4E'
else:
    SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '124084319215-hf6ir00ju83rpibpk1f3rc3bcjubsong.apps.googleusercontent.com'
    SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'kqcOr1_M0QxVfG6MfmDX40KD'

if PRODUCTION:
    SOCIAL_AUTH_FACEBOOK_KEY = '792613207533003'
    SOCIAL_AUTH_FACEBOOK_SECRET = 'f919160722f4ba9b3a489216b27c4942'
else:
    SOCIAL_AUTH_FACEBOOK_KEY = '793207680806889'
    SOCIAL_AUTH_FACEBOOK_SECRET = '23540deabdeb26ad5f43cd8bf4b9704e'

if PRODUCTION:
    SOCIAL_AUTH_TWITTER_KEY = 'nlO0qVuopGLcXBNmsi1rC7pbD'
    SOCIAL_AUTH_TWITTER_SECRET = 'ai0WpB4vqxgRnjMCk8ZDpgGg6bY89k7an0DjgSjuKRW86nXpXv'
else:
    SOCIAL_AUTH_TWITTER_KEY = '6egWbDD58Ect86uObQfzbZ0WP'
    SOCIAL_AUTH_TWITTER_SECRET = 'oHURzyf259OsbTNQMpzJwo7wCiwUxNkZVOGQVdgBYnQuWs0YPl'

if PRODUCTION:
    SOCIAL_AUTH_VK_OAUTH2_KEY = '5075509'
    SOCIAL_AUTH_VK_OAUTH2_SECRET = 'brAPzXbKEerSFmUQLC3k'
else:
    SOCIAL_AUTH_VK_OAUTH2_KEY = '5075860'
    SOCIAL_AUTH_VK_OAUTH2_SECRET = 'qKyHnjPWtcVsWcKEH6Z7'


AUTHENTICATION_BACKENDS = (
    'social.backends.google.GoogleOAuth2',
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.twitter.TwitterOAuth',
    'social.backends.vk.VKOAuth2',
    'django.contrib.auth.backends.ModelBackend'
)

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'

SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/'
SOCIAL_AUTH_LOGIN_ERROR_URL = '/login-error/'
SOCIAL_AUTH_LOGIN_URL = '/login/'
SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/'
SOCIAL_AUTH_NEW_ASSOCIATION_REDIRECT_URL = '/'
SOCIAL_AUTH_DISCONNECT_REDIRECT_URL = '/'
SOCIAL_AUTH_INACTIVE_USER_URL = '/inactive/'

SOCIAL_AUTH_STRATEGY = 'social.strategies.django_strategy.DjangoStrategy'
SOCIAL_AUTH_STORAGE = 'social.apps.django_app.default.models.DjangoStorage'
SOCIAL_AUTH_GOOGLE_OAUTH_SCOPE = [
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/userinfo.profile'
]
# SOCIAL_AUTH_EMAIL_FORM_URL = '/signup-email'
#SOCIAL_AUTH_EMAIL_FORM_HTML = 'email_signup.html'
#SOCIAL_AUTH_EMAIL_VALIDATION_FUNCTION = 'example.app.mail.send_validation'
#SOCIAL_AUTH_EMAIL_VALIDATION_URL = '/email-sent/'
# SOCIAL_AUTH_USERNAME_FORM_URL = '/signup-username'
SOCIAL_AUTH_USERNAME_FORM_HTML = 'username_signup.html'

SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.user.get_username',
    #'social.pipeline.mail.mail_validation',
    'social.pipeline.social_auth.associate_by_email',
    'social.pipeline.user.create_user',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details'
)

SOCIAL_AUTH_ADMIN_USER_SEARCH_FIELDS = ['username', 'email']

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'social.apps.django_app.default',
    'rest_framework',
    'app'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'plotyourthought.urls'

WSGI_APPLICATION = 'plotyourthought.wsgi.application'


# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

if PRODUCTION:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'plotyourthought',
            'USER': 'postgres',
            'PASSWORD': 'coddacidcrudrest',
            'HOST': 'localhost'
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'develop_plotyourthought',
            'USER': 'postgres',
            'PASSWORD': 'coddacidcrudrest',
            'HOST': 'localhost'
        }
    }

# Internationalization
# https://docs.djangoproject.com/en/dev/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "plotyourthought", "static", "dist"),
)

MEDIA_ROOT = os.path.join(os.path.dirname(__file__), "media")
MEDIA_URL = '/media/'