from django.test import TestCase

from django.utils import timezone
from django.contrib.auth.hashers import make_password
from django.core.urlresolvers import reverse
from django.core.files import File
from django.conf import settings

from rest_framework.test import APIClient, APITestCase

from app.models import *

def admin_setup():
	admin = User(username='admin', is_staff=True, is_superuser=True)
	admin_password = 'admin'
	admin.set_password(admin_password)
	admin.save()
	return (admin, admin_password)

def test_get(test_case, new_model_instance, url_list, url_detail, admin, admin_password):
	'''
	Tests that REST API endpoint correctly replies to GET request.

	Arguments:
		test_case - TestCase instance, where this function is called.
		new_model_instance - model instance, created during setUp(), e.g. User.
		url_list - str, representing url of collecion, e.g. '/api/user/'.
		url_detail - str, representing url of a single instance of model, e.g. '/api/user/911'.
		admin - admin user object
		admin_password - admin password string (raw, not password hash)
	'''

	c = APIClient()

	response = c.get(url_list)
	if response.status_code != 200: print response.content
	test_case.assertEqual(response.status_code, 200)

	response = c.get(url_detail)
	if response.status_code != 200: print response.content
	test_case.assertEqual(response.status_code, 200)

	# test logged-in
	login_status = c.login(username=admin.username, password=admin_password)
	test_case.assertTrue(login_status)

	response = c.get(url_list)
	if response.status_code != 200: print response.content
	test_case.assertEqual(response.status_code, 200)

	response = c.get(url_detail)
	if response.status_code != 200: print response.content
	test_case.assertEqual(response.status_code, 200)


def test_no_post_put_delete(test_case, new_model_instance, url_list, url_detail, admin, admin_password):
	'''
	This function tests that REST API endpoints, given by url_list and url_detail,
	can not be modified by POST, PUT or DELETE requests.

	Arguments:
		test_case - TestCase instance, where this function is called.
		new_model_instance - model instance, created during setUp(), e.g. User.
		url_list - str, representing url of collection, e.g. '/api/user/'.
		url_detail - str, representing url of a single instance of model, e.g. '/api/user/911'
		admin - admin user object
		admin_password - admin password string (raw, not password hash)
	'''

	c = APIClient()

	response = c.post(url_list, {})
	test_case.assertEqual(response.status_code, 403)

	response = c.delete(url_detail)
	test_case.assertEqual(response.status_code, 403)

	response = c.put(url_detail, {})
	test_case.assertEqual(response.status_code, 403)

	# test logged-in
	login_status = c.login(username=admin.username, password=admin_password)
	test_case.assertTrue(login_status)

	response = c.post(url_list, {})
	test_case.assertEqual(response.status_code, 405)

	response = c.delete(url_detail)
	test_case.assertEqual(response.status_code, 405)

	response = c.put(url_detail, {})
	test_case.assertEqual(response.status_code, 405)


class UserTestCase(APITestCase):
	def setUp(self):
		self.admin, self.admin_password = admin_setup()

		self.new_user = User(username="1234567", password="1234567", is_staff=False, is_superuser=False)
		self.new_user.save()

	def test_get(self):
		url_list = reverse("api:user-list", kwargs={})
		url_detail = reverse("api:user-detail", kwargs={'username': self.new_user.username})

		test_get(self, self.new_user, url_list, url_detail, self.admin, self.admin_password)

	def test_post_put_delete(self):
		url_list = reverse("api:user-list", kwargs={})
		url_detail = reverse("api:user-detail", kwargs={'username': self.new_user.username})

		test_no_post_put_delete(self, self.new_user, url_list, url_detail, self.admin, self.admin_password)

class PostTestCase(APITestCase):
	def setUp(self):
		self.admin, self.admin_password = admin_setup()

		self.new_post = Post(author=self.admin, title="new post")
		self.new_post.save()

		self.new_cathegory = Cathegory(name="new cathegory")
		self.new_cathegory.save()

		self.new_post.cathegories.add(self.new_cathegory)
		self.new_post.save()

	def test_get(self):
		url_list = reverse("api:post-list", kwargs={})
		url_detail = reverse("api:post-detail", kwargs={'pk': self.new_post.pk})

		test_get(self, self.new_post, url_list, url_detail, self.admin, self.admin_password)

	def test_post_put_delete(self):
		url_list = reverse("api:post-list", kwargs={})
		url_detail = reverse("api:post-detail", kwargs={'pk': self.new_post.pk})

		test_no_post_put_delete(self, self.new_post, url_list, url_detail, self.admin, self.admin_password)

class PostFragmentTestCase(APITestCase):
	def setUp(self):
		self.admin, self.admin_password = admin_setup()

		self.new_post = Post(author=self.admin, title="new post")
		self.new_post.save()

		self.new_post_fragment = PostFragment(markdown="", fabric="", content_type="fabric", post=self.new_post, index=0)
		self.new_post_fragment.save()

	def test_get(self):
		url_list = reverse("api:postfragment-list", kwargs={})
		url_detail = reverse("api:postfragment-detail", kwargs={'pk': self.new_post_fragment.pk})

		test_get(self, self.new_post, url_list, url_detail, self.admin, self.admin_password)

	def test_post_put_delete(self):
		url_list = reverse("api:postfragment-list", kwargs={})
		url_detail = reverse("api:postfragment-detail", kwargs={'pk': self.new_post_fragment.pk})

		test_no_post_put_delete(self, self.new_post, url_list, url_detail, self.admin, self.admin_password)

class CathegoryTestCase(APITestCase):
	def setUp(self):
		self.admin, self.admin_password = admin_setup()

		self.new_cathegory = Cathegory(name="new cathegory")
		self.new_cathegory.save()

	def test_get(self):
		url_list = reverse("api:cathegory-list", kwargs={})
		url_detail = reverse("api:cathegory-detail", kwargs={'pk': self.new_cathegory.pk})

		test_get(self, self.new_cathegory, url_list, url_detail, self.admin, self.admin_password)

	def test_post_put_delete(self):
		url_list = reverse("api:cathegory-list", kwargs={})
		url_detail = reverse("api:cathegory-detail", kwargs={'pk': self.new_cathegory.pk})

		test_no_post_put_delete(self, self.new_cathegory, url_list, url_detail, self.admin, self.admin_password)