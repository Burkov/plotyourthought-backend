from django.conf.urls import patterns, include, url

from rest_framework.routers import DefaultRouter

from app.views import *

router = DefaultRouter()
router.register(r'post', APIPostViewSet)
router.register(r'postfragment', APIPostFragmentViewSet)
router.register(r'cathegory', APICathegoryViewSet)
router.register(r'user', APIUserViewSet)


urlpatterns = patterns('',
    url(r'^$',
        index_view,
        {},
        name='index'),
    url(r'^blog/$',
        blog_view,
        {},
        name='blog'),
    url(r'^edit_blog/$',
        edit_blog_view,
        {},
        name='edit_blog'),
    url(r'^profile/(?P<username>[\w.@+-]+)/$',
        profile_view,
        {},
        name='profile'),
    url(r'^account/(?P<username>[\w.@+-]+)/$',
        account_view,
        {},
        name='account'),

    # login views
    url(r'^email-sent/(?P<username>[\w.@+-]+)/$',
        email_sent_view,
        {},
        name='email_sent'),
    url(r'^email_confirmation/(?P<username>[\w.@+-]+)/(?P<code>[\w_-]+)/$',
        email_confirmation_view,
        {},
        name='email_confirmation'),    
    url(r'^logout/$',
        logout_view,
        {},
        name='logout'),
    url(r'^recover_password/$',
        recover_password_view,
        {},
        name='recover_password'),

    # API urls
    url(r'^api/', include(router.urls, namespace="api"))
)