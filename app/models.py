import uuid
import base64

import jsonfield
from markupfield.fields import MarkupField

from django.db import models
from django.contrib.auth.models import User, Permission, BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

class UserManager(BaseUserManager):
    def create_user(self, username, email, password=None):
        user = self.model(username=username, email=email, last_login=timezone.now())
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        user = self.create_user(username=username, email=None, password=password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

def generate_registration_code():
    return base64.urlsafe_b64encode(uuid.uuid4().bytes).strip("=")

class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(_('username'), max_length=255, unique=True)
    # don't need password and last_login fields, cause they are already defined in AbstractBaseUser
    # is_active is just True in AbstractBaseUser, but we'd want a field for it.
    email = models.EmailField(null=True, blank=True)
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_('Designates whether this user should be treated as active. Unselect this instead of deleting accounts.')
    )
    is_staff = models.BooleanField(
        _('staff status'), 
        default=False,
        help_text=_('Designates whether the user can log into this admin site.')
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    code = models.CharField(null=True, blank=True, default=generate_registration_code, max_length=255, help_text=_('Code to be sent via e-mail upon register or password recovery.'))
    new_password = models.CharField(null=True, blank=True, default="", max_length=255, help_text=_('If user is attempting to change password, this field stores new password until confirmation code is entered'))

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.username

    def get_short_name(self):
        return self.username

    def get_full_name(self):
        return "%s (%s %s)" % (self.username, self.first_name, self.last_name)

class Post(models.Model):
    author = models.ForeignKey(User)
    title = models.TextField(null=False, blank=False)
    date = models.DateTimeField(_('date'), default=timezone.now)
    cathegories = models.ManyToManyField("Cathegory")

class PostFragment(models.Model):
    content_type_choices = (('markdown', 'markdown'), ('fabric', 'fabric'))

    markdown = MarkupField(markup_type='markdown')
    fabric = jsonfield.JSONField()
    content_type = models.CharField(max_length=10, choices=content_type_choices, default='markdown')
    post = models.ForeignKey(Post)
    index = models.IntegerField(null=False, blank=False)

class Cathegory(models.Model):
    name = models.CharField(max_length=256)
    parent = models.ForeignKey("Cathegory", null=True, blank=True)

#class Analytics(models.Model):
#    pass
#
#class Attachment(models.Model):
#    name = models.CharField(max_length=255, default="")
#    attachment = models.ImageField()
#
#class ThoughtUser(models.Model):
#    avatar = models.ForeignKey(Attachment)
#    wallet_id = models.CharField(max_length=255)
#
#class TranslationsSet(models.Model):
#    pass
#
#class Language(models.Model):
#    name = models.CharField(max_length=255)
#
#class Translation(models.Model):
#    translation_set = models.ForeignKey(TranslationsSet)
#    language = models.ForeignKey(Language)
#    translation = models.TextField(default="")
#
#class Content(models.Model):
#    content = models.TextField(default="")
#
#class Page(models.Model):
#    title = models.CharField(max_length=1023, default="")
#    user = models.ForeignKey(User)
#    monetized = models.BooleanField(default=False)
#    permissions = models.ManyToManyField(Permission)
#    analytics = models.ForeignKey(Analytics)
#    depends = models.ManyToManyField("self", symmetrical=False)
#
#class Revision(models.Model):
#    user = models.ForeignKey(User)
#    page = models.ForeignKey(Page)
#    date = models.DateTimeField()
#    content = models.ForeignKey(Content)
