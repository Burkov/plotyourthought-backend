import sendgrid

#from django.shortcuts import render

from django.conf import settings

from django.http import HttpResponse, HttpResponseRedirect, Http404, HttpResponseForbidden
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.core.urlresolvers import reverse

from django.utils.http import is_safe_url

from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.views.decorators.http import require_POST

from django.contrib.auth import logout as auth_logout, authenticate, login
from django.contrib.sites.shortcuts import get_current_site

from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import status
from rest_framework.response import Response
from rest_framework import filters

from social.backends.oauth import BaseOAuth1, BaseOAuth2
from social.backends.google import GooglePlusAuth
from social.backends.utils import load_backends
from social.apps.django_app.utils import psa

from forms import UserCreationForm, AuthenticationForm, PasswordRecoveryForm
DEBUG = settings.DEBUG

from models import *
import serializers

##### Authentication-related views #####

# login view is imported straight from django.contrib.auth
#def login(request):
#   pass

def logout_view(request):
    auth_logout(request)
    return redirect('/')

def send_registration_email(request, new_user):
    hyperlink = create_register_hyperlink(request, new_user)

    client = settings.SEND_GRID
    message = sendgrid.Mail()

    message.add_to(new_user.email) # add recepient (there can be multiple)
    message.set_from("no-reply@plotyourthought.com")
    message.set_subject(u'Registration on PlotYourThought.com')
    message.set_html(u'''
    <html>
        <head>
            <title>Registration e-mail from PlotYourThought.com</title>
        </head>
        <body>
            <h1>Registration on PlotYourThought</h1>
            <p>You (or someone else) are creating an account on PlotYourThought.com, associated
            with this e-mail address.
            Click on the link below to accomplish registration and log into Your account.
            </p>
            <a href="%s">Click here to accomplish registration</a>
        </body>
    </html>
    ''' % hyperlink)

    client.send(message)
    # redirect user to the email sent page
    redirect_to = reverse("email_sent", kwargs={"username": new_user.username})
    return HttpResponseRedirect(redirect_to)

def create_register_hyperlink(request, user):
    return "https://%s%s" % (get_current_site(request).name, reverse("email_confirmation", kwargs={"username": user.username, "code": user.code}))

@sensitive_post_parameters('password1', 'password2', 'password')
@csrf_protect
def email_sent_view(request, username):
    """A message page, shown when user first registered or when user requested password recovery.
    """
    if not request.user.is_anonymous():
        return HttpResponseRedirect(reverse("profile", kwargs={"username":request.user.username}))

    context = {
        'username': username,
        'site': get_current_site(request),
        'site_name': get_current_site(request).name,
    }

    topbar_result = process_unauthenticated_topbar(request, context)
    if type(topbar_result) == HttpResponseRedirect:
        return topbar_result

    return TemplateResponse(request, 'email_sent.html', context)

def email_confirmation_view(request, username, code):
    """
    Performs a password-less login for each of 2 scenarios:
    1) User first registered on the website, gets an e-mail with link with validation code
        and follows that link. The link leads here, and this view makes user account active.
    2) User (or someone else) sent request for password recovery and if user followed that
        link, he comes here and is logged-in.
    """
    # we don't allow already logged-in user to this page
    if not request.user.is_anonymous():
        return HttpResponseRedirect(reverse("profile", kwargs={"username":request.user.username}))

    redirect_to = request.POST.get('next',
                                   request.GET.get('next', ''))

    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        # logger.error(traceback.format_exc(sys.exc_info()[2]))        
        raise Http404

    # if code is empty or wrong, kick him
    if user.code == "" or user.code != code:
        return HttpResponseForbidden("Activation code in the hyperlink is wrong or outdated.")

    # the code is right, activate user account, if it's not active yet
    if not user.is_active: user.is_active = True
    # code is one-time only, clear it
    user.code = ""
    # if user was changing password, set his new password
    if user.new_password != "":
        user.set_password(user.new_password)
        user.new_password = ""
    user.save()
    # don't call authenticate, instead set authentication backend manually so that django doesn't complain
    user.backend = 'django.contrib.auth.backends.ModelBackend'
    login(request, user)
    # redirect freshly registered user to his profile page
    return HttpResponseRedirect(reverse("profile", kwargs={"username":user.username}))

@csrf_exempt
@require_POST
def recover_password_view(request):
    # works with page reload and redirects back to the calling page
    if not request.user.is_anonymous:
        return HttpResponseBadRequest("You're already logged in! Just change your password, don't recover it.")

    password_recovery_form = PasswordRecoveryForm(request.POST) # create bound UserCreationForm
    if password_recovery_form.is_valid(): # if it really is a valid e-mail of existing user
        username = password_recovery_form.cleaned_data['username']
        user = User.objects.get(username=username) # no try here, else form would've been invalid
        user.code = make_random_password(length=32)
        user.new_password = make_random_password(length=32)
        user.save()

        hyperlink = create_login_hyperlink(request, user)

        client = settings.SEND_GRID
        message = sendgrid.Mail()

        message.add_to(username) # add recepient (there can be multiple)
        message.set_from("no-reply@plotyourthought.com")
        message.set_subject(u'Password recovery')
        message.set_html(u'''
        <html>
            <head>
                <title>Password recovery e-mail from PlotYourThought.com</title>
            </head>
            <body>
                <h1>Password recovery at PlotYourThought.com</h1>
                <p>You (or someone else) have requested password recovery for your account at PlotYourThought.com.</p>
                <br>
                <p>Follow the link below to set a temporary password for your account: %s.</p>
                <br>
                <p>You'll be logged in to the site without entering new password. To change the password,
                visit your account page.
                </p>
                <a href="%s">Click here to enter PlotYourThought.com without a password.</a>
            </body>
        </html>
        ''' % (user.new_password, hyperlink))

        client.send(message)
        # redirect user to the email sent page
        redirect_to = reverse("email_sent", kwargs={"username": user.username})

        return HttpResponseRedirect(redirect_to)


##### REST Api #####

class APIUserViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAdminUser, )
    queryset = User.objects.all()
    lookup_field = 'username'
    serializer_class = serializers.UserSerializer

    # Warning: 
    # Don't call this method get_object as it is done for APIView!
    # For APIViewSets it will be called by RetrieveModelMixin.retrieve(object)
    # and result in an error.
    def get_one(self, username):
        try:
            user = User.objects.get(username=username)
            return user
        except User.DoesNotExist:
            raise Http404

    def detail(self, request, username, format=None):
        user = self.get_one(username=username)
        serializers = self.serializer(user, context={'request': request})
        return Response(serializer.data)

    def update(self, request, username, format=None):
        user = self.get_one(username=username)
        serializers = self.serializer(user, data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, username, format=None):
        user = self.get_one(username=username)
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class APIPostViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )
    queryset = Post.objects.all()
    lookup_field = 'pk'

    def get_serializer_class(self):
        return serializers.PostSerializer
        # if you need different serializers for different http requests, use this:
        #if self.action =='list' or self.action == 'retreive' or self.action == 'put' or self.action == 'delete':        

    def get_one(self, pk):
        try:
            post = Post.objects.get(pk=pk)
            return post
        except Post.DoesNotExist:
            raise Http404

    def detail(self, request, pk, format=None):
        print "request = %s, pk = %s, format=%s" % (requets, pk, format)
        post = self.get_one(pk)
        serializer = self.get_serializer_class()(post, context={'request': request})
        return Response(serializer.data)

    def list(self, request, format=None):
        print "list called"
        post = Post.objects.all()
        serializer = self.get_serializer_class()(post, many=True, context={'request': request})
        return Response(serializer.data)

    def create(self, request, format=None):
        serializer = self.get_serializer_class()(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk, format=None):
        post = self.get_one(pk)
        serializer = self.get_serializer_class()(post, data=request.data, context={'request', request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk, format=None):
        post = self.get_one(pk)
        post.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class APIPostFragmentViewSet(viewsets.ModelViewSet):
    filter_backends = (filters.DjangoFilterBackend, )
    filter_fields = ('post', )
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )
    queryset = PostFragment.objects.all()
    serializer_class = serializers.PostFragmentSerializer

class APICathegoryViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, permissions.IsAdminUser)
    queryset = Cathegory.objects.all()
    serializer_class = serializers.CathegoriesSerializer

##### Pages #####

def index_view(request):
	context = {
		'plud_id': getattr(settings, 'SOCIAL_AUTH_GOOGLE_PLUS_KEY', None),
		'plus_scope': ''.join(GooglePlusAuth.DEFAULT_SCOPE),
		'available_backends': load_backends(settings.AUTHENTICATION_BACKENDS)
	}

	topbar_result = process_unauthenticated_topbar(request, context)
	if (type(topbar_result)) == HttpResponseRedirect:
		return topbar_result

	return TemplateResponse(request, 'index.html', context)


def process_unauthenticated_topbar(request, context):
    '''Call this function in each view that has topbar with login and register
     forms to process the data of those forms.

    Returns HttpResponseRedirect, if user decides to login.
    
    Modifies context, adding the following names to it:
        "auth_form"
        "user_creation_form"
        "next"
    '''
    redirect_to = request.POST.get("next", request.GET.get("next", ''))

    if DEBUG: print "request.method = %s" % request.method
    if request.method == "POST":
        if DEBUG: print "process_authentication_tobar_forms: request.POST = %s" % request.POST
        # check, which submit button was pressed; parse the respective form
        if "Create account" in request.POST: # if user's attempting to register
            auth_form = AuthenticationForm() # create auth form unbound
            password_recovery_form = PasswordRecoveryForm() # create password recovery form unbound
            user_creation_form = UserCreationForm(request.POST) # create bound UserCreationForm
            if user_creation_form.is_valid():
                new_user = user_creation_form.save() # create new user account with is_active=False and random code for account activation
                send_registration_email(request, new_user)
                # redirect user to the email sent page
                redirect_to = reverse("email_sent", kwargs={"username": new_user.username})
                return HttpResponseRedirect(redirect_to)

        elif "Login" in request.POST: # if user is trying to login
            user_creation_form = UserCreationForm() # create user_creation_form unbound
            password_recovery_form = PasswordRecoveryForm() # create password recovery form unbound
            auth_form = AuthenticationForm(request, data=request.POST) # create bound AuthenticationForm
            if auth_form.is_valid():
                redirect_to = request.POST.get("next", '')
                # if redirection url is empty, return to the same page, we came from
                if not request.POST.get("next", ''):
                    redirect_to = request.path
                # Ensure the user-originating redirection url is safe.
                if not is_safe_url(url=redirect_to):
                    redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)
                # Okay, security check complete. Log the user in.
                login(request, auth_form.get_user())
                return HttpResponseRedirect(redirect_to)
        else: # none of topbar forms was submitted, create all forms unbound
            user_creation_form = UserCreationForm()
            auth_form = AuthenticationForm()
            password_recovery_form = PasswordRecoveryForm()
    else: # if it's an idempotent request (GET etc.), create all forms unbound
        auth_form = AuthenticationForm()
        user_creation_form = UserCreationForm()
        password_recovery_form = PasswordRecoveryForm()

    context["auth_form"] = auth_form
    context["user_creation_form"] = user_creation_form
    context["password_recovery_form"] = password_recovery_form
    context["next"] = redirect_to

def profile_view(request, username):
    context = {}
    return TemplateResponse(request, 'profile.html', context)

def account_view(request, username):
    context = {}
    return TemplateResponse(request, 'account.html', context)

def blog_view(request):
    context = {}
    return TemplateResponse(request, 'blog.html', context)

def edit_blog_view(request):
    context = {}
    return TemplateResponse(request, 'edit_blog.html', context)