import os
import re

from collections import OrderedDict
import uuid
import base64

from django import forms
from django.utils.translation import ugettext, ugettext_lazy as _
from django.utils.text import capfirst
from django.contrib.auth.hashers import make_password
from django.contrib.auth import authenticate, get_user_model

from models import User
from django.conf import settings

def make_random_password(length=10, allowed_chars='abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789'):
    '''
    Stolen from mongoengine, as User.objects.make_random_password() doesn't work:
    'QuerySet' object has no attribute 'make_random_password'.
    "Generates a random password with the given length and given allowed_chars"
    '''
    # Note that default value of allowed_chars does not have "I" or letters
    # that look like it -- just to avoid confusion.
    from random import choice
    return ''.join([choice(allowed_chars) for i in range(length)])


# UserCreationForm, AuthenticationForm and SetPasswordForm are stolen from
# django.contrib.auth.forms almost verbatim with modifications 
# having to do with:
# 1) styling widgets with our CSS styles

class UserCreationForm(forms.Form):
    error_messages = {
        'duplicate_email': _("A user with that e-mail already exists."),
        'password_mismatch': _("The two password fields didn't match."),
    }

    email = forms.RegexField( label=_("E-mail"),
                              regex=re.compile("^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", flags=re.IGNORECASE),
                              help_text=_("Required. E-mail. Letter, digits @/./+/-/_ characters only."),
                              error_messages={'invalid': _("This value should be an e-mail address like 'John.Smith@mail-service.com'. It may contain only letters, numbers and @/./+/-/_ characters.")},
                              widget=forms.TextInput(attrs={
                                'id':'register-email',
                                'placeholder': _("E-mail"),
                                'class': 'form-control'}),
                              required=True)
    password1 = forms.CharField( label=_("Password"),
                                 widget=forms.PasswordInput(attrs={
                                    'id':'register-pass',
                                    'placeholder': _("Password"),
                                    'class': 'form-control'})
                               )
    password2 = forms.CharField( label=_("Password confirmation"),
                                 widget=forms.PasswordInput(attrs={
                                    'id':'register-pass2',
                                    'placeholder': _("Password confirmation"),
                                    'class': 'form-control'}),
                                 help_text="Enter the same password as above, for verification."
                               )
    def clean_email(self):
        email = self.cleaned_data["email"]
        email = email.lower()
        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError(self.error_messages['duplicate_email'], code='duplicate_email')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(self.error_messages['password_mismatch'], code='password_mismatch')
        return password2

    def save(self):
        #TODO django.contrib.auth.forms.UserCreationForm had a commit option. What do i do with it?
        username = base64.urlsafe_b64encode(uuid.uuid4().bytes).strip("=")
        email = self.cleaned_data["email"]
        password = make_password(self.cleaned_data["password1"])

        code = make_random_password(length=32)
        new_user = User(username=username, email=email, password=password)
        new_user.is_active = False        
        new_user.save()

        return new_user


class AuthenticationForm(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    email = forms.RegexField( label=_("E-mail"),
                              regex=re.compile("^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", flags=re.IGNORECASE),
                              help_text=_("Required. E-mail. Letter, digits and @/./+/-/_ characters only."),
                              error_messages={'invalid': _("This value may contain only letters, numbers and @/./+/-/_ characters.")},
                              widget=forms.TextInput(attrs={
                                'placeholder': _("E-mail"),
                                'id':'auth-email',
                                'class':'form-control'
                              }),
                              required=True)
    password = forms.CharField( label=_("Password"), 
                                widget=forms.PasswordInput(attrs={
                                        'placeholder': _("Password"),
                                        'id': 'auth-pass',
                                        'class': 'form-control'
                                }),
                                required=True)

    error_messages = {
        'invalid_login': _("Please enter correct e-mail and password. "
                           "Note that password may be case-sensitive."),
        'inactive': _("This account is inactive."),
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super(AuthenticationForm, self).__init__(*args, **kwargs)

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        if email and password:
            try:
                username = User.objects.get(email=email).username
            except User.DoesNotExist:
                raise forms.ValidationError(self.error_messages['invalid_login'], code='invalid_login')
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'email': self.email.verbose_name},
                )
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active users, and reject login by inactive users.

        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.

        If the given user may log in, this method should return None.
        """
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache

class SetPasswordForm(forms.Form):
    """
    A form that lets a user set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }

    new_password1 = forms.CharField(label=_("New password"),
                                    widget=forms.PasswordInput(attrs={'id':'change-newpass', 'placeholder': _("Password")}))
    new_password2 = forms.CharField(label=_("New password confirmation"),
                                    widget=forms.PasswordInput(attrs={'id':'change-newpass2', 'placeholder': _("Password confirmation")}))

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(SetPasswordForm, self).__init__(*args, **kwargs)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        return password2

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['new_password1'])
        if commit:
            self.user.save()
        return self.user


class PasswordChangeForm(SetPasswordForm):
    """
    A form that lets a user change his/her password by entering
    their old password.
    """
    error_messages = dict(SetPasswordForm.error_messages, **{
        'password_incorrect': _("Your old password was entered incorrectly. "
                                "Please enter it again."),
    })
    old_password = forms.CharField(label=_("Old password"),
                                   widget=forms.PasswordInput(attrs={'id': 'change-pass', 'placeholder': _("Old password")}))

    def clean_old_password(self):
        """
        Validates that the old_password field is correct.
        """
        old_password = self.cleaned_data["old_password"]
        if not self.user.check_password(old_password):
            raise forms.ValidationError(
                self.error_messages['password_incorrect'],
                code='password_incorrect',
            )
        return old_password

PasswordChangeForm.base_fields = OrderedDict(
    (k, PasswordChangeForm.base_fields[k])
    for k in ['old_password', 'new_password1', 'new_password2']
)


class PasswordRecoveryForm(forms.Form):
    """
    A form that sends a password recovery e-mail to the user.
    """
    # TODO check that user account is active
    error_messages = {'non_existing_user': _("Please enter a correct e-mail of an existing user account.")}

    email = forms.RegexField(label=_("E-mail"),
                regex=re.compile("^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", flags=re.IGNORECASE),
                help_text=_("Required. E-mail. Letter, digits"
                " and @/./+/-/_ characters only."),
                error_messages={'invalid': _("This value may contain only letters, numbers and @/./+/-/_ characters.")},
                widget=forms.TextInput(attrs={"id": "forgot-pass-email", "placeholder": _("E-mail")}))

    def clean_email(self):
        email = self.cleaned_data["email"]
        email = email.lower()
        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            raise forms.ValidationError(self.error_messages['non_existing_user'], code='non_existing_user')
        return email